$(document).ready(function() {
    const urlParams = new URLSearchParams(window.location.search);
    const username = urlParams.get('id');
  
    if (!username) {
      // No username provided in URL, redirect to login page
      window.location.href = 'login.html';
    }
  
    // Display the username
    $('#username').text(username);
  
    // Load user's score on page load
    async function loadUserScore() {
      try {
        const response = await fetch(`http://basic-web.dev.avc.web.usf.edu/${username}`, { method: 'GET' });
  
        if (response.status === 200) {
          const userData = await response.json();
          updateScoreDisplay(userData.score); // Update score display if user exists
        } else if (response.status === 404) {
          await createUser(username); // Create user if not found
        } else if (response.status === 500) {
          const errorData = await response.json();
          alert(`Internal server error: ${errorData.Error}`);
        } else {
          throw new Error('Error loading score');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
  
    // Create user on the server with initial score 0
    async function createUser(username) {
      try {
        const response = await fetch(`http://basic-web.dev.avc.web.usf.edu/${username}`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({ score: 0 }) // Create user with initial score 0
        });
  
        if (response.status === 200 || response.status === 201) {
          const userData = await response.json();
          updateScoreDisplay(userData.score); // Update score display after creating user
        } else if (response.status === 400) {
          const errorData = await response.json();
          alert(`Invalid request: ${errorData.Error}`);
        } else if (response.status === 500) {
          const errorData = await response.json();
          alert(`Internal server error: ${errorData.Error}`);
        } else {
          throw new Error('Error creating user');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
  
    // Increment score and update display
    $('#increment-btn').click(async function() {
      try {
        const response = await fetch(`http://basic-web.dev.avc.web.usf.edu/${username}`, { method: 'GET' });
  
        if (response.status === 200) {
          const userData = await response.json();
          const newScore = calculateNewScore(userData.score); // Calculate new score
          await updateScore(newScore); // Update score on server
        } else if (response.status === 404) {
          alert('User not found.');
        } else if (response.status === 500) {
          const errorData = await response.json();
          alert(`Internal server error: ${errorData.Error}`);
        } else {
          throw new Error('Error loading score');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    });
  
    // Calculate new score
    function calculateNewScore(currentScore) {
      return currentScore + 1; // Increment score by 1
    }
  
    // Update user's score on the server and display
    async function updateScore(newScore) {
      try {
        const response = await fetch(`http://basic-web.dev.avc.web.usf.edu/${username}`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({ score: newScore }) // Update user score
        });
  
        if (response.status === 200 || response.status === 201) {
          const userData = await response.json();
          updateScoreDisplay(userData.score); // Update display with new score
        } else if (response.status === 400) {
          const errorData = await response.json();
          alert(`Invalid request: ${errorData.Error}`);
        } else if (response.status === 500) {
          const errorData = await response.json();
          alert(`Internal server error: ${errorData.Error}`);
        } else {
          throw new Error('Error updating score');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
  
    // Update score display
    function updateScoreDisplay(score) {
      let displayText = '';
      if (score % 3 === 0 && score % 5 === 0 && score !== 0) {
        displayText = 'FizzBuzz'; // Display FizzBuzz for multiples of 3 and 5
      } else if (score % 3 === 0 && score !== 0) {
        displayText = 'Fizz'; // Display Fizz for multiples of 3
      } else if (score % 5 === 0 && score !== 0) {
        displayText = 'Buzz'; // Display Buzz for multiples of 5
      } else {
        displayText = score.toString(); // Display the actual number if not Fizz, Buzz, or FizzBuzz
      }
      $('#score-display').text(displayText);
    }
  
    // Load user's score when the page loads
    loadUserScore();
  });
  