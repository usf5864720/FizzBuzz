

#### Overview:
This project implements a simple Fizzbuzz challenge with user login system and using an API endpoint to check and create users on the server. It comprises two main HTML pages (`login.html` and `main.html`) along with corresponding JavaScript files (`login.js` and `main.js`). The CSS styles are defined in `styles.css`.

#### Functions and Methods Used:

##### login.js:
1. **`$(document).ready(function() { ... })`**:
   - This function ensures that the JavaScript code inside it runs after the document is fully loaded.

2. **`$('#login-form').submit(async function(e) { ... })`**:
   - Handles the submission of the login form.
   - Uses `async/await` to handle asynchronous operations (like API requests) inside the form submission.

3. **`async function checkUsernameAndRedirect(username) { ... }`**:
   - Checks if the entered username exists on the server using a GET request to the API.
   - Redirects the user to the main page (`main.html`) if the username exists.
   - Creates a new user with an initial score of 0 if the username does not exist.
   - Handles different HTTP response statuses (200, 404, 500) appropriately.

4. **`async function createUser(username) { ... }`**:
   - Sends a POST request to the API to create a new user with an initial score of 0.
   - Handles different HTTP response statuses (200, 201, 400, 500) appropriately.

##### main.js:
1. **`$(document).ready(function() { ... })`**:
   - Similar to the one in `login.js`, ensures that JavaScript code runs after page load.

2. **`async function loadUserScore() { ... }`**:
   - Loads the user's score from the server on page load using a GET request to the API.
   - Updates the score display based on the returned data.

3. **`async function updateScore(newScore) { ... }`**:
   - Sends a POST request to the API to update the user's score.
   - Calculates the new score, updates it on the server, and updates the display accordingly.
   - Handles different HTTP response statuses (200, 201, 400, 500) appropriately.

4. **`$('#increment-btn').click(async function() { ... })`**:
   - Handles the click event of an increment button to increase the user's score.
   - Calls `updateScore` function to update the score on the server and display.

5. **`function updateScoreDisplay(score) { ... }`**:
   - Updates the score display based on the provided score value.
   - Displays "Fizz", "Buzz", or "FizzBuzz" for specific score conditions.
   - Updates the DOM element to reflect the new score display.

#### Why Async Functions Were Used:
- Async functions were used to handle asynchronous operations such as API requests (`fetch` calls).
- Asynchronous programming ensures that the application remains responsive during these operations, preventing blocking of the UI.

#### API Usage:
- Both `login.js` and `main.js` interact with an API hosted at `http://basic-web.dev.avc.web.usf.edu/` to perform user-related operations (checking existence, creating users, updating scores).

#### Methods Overview:
- `GET` requests are used to check if a username exists and to load user scores.
- `POST` requests are used to create new users and update user scores.
- Different API response statuses (200, 201, 400, 404, 500) are handled to provide appropriate feedback to the user.

This setup ensures a responsive and interactive user experience for logging in, managing user data, and updating scores within the web application.