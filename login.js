$(document).ready(function() {
  // Handle login form submission
  $('#login-form').submit(async function(e) {
    e.preventDefault();
    const username = $('#username').val().trim();
    if (username) {
      await checkUsernameAndRedirect(username); // Check username and redirect if valid
    } else {
      alert('Please enter a username.');
    }
  });

  // Check if username exists on the server and redirect accordingly
  async function checkUsernameAndRedirect(username) {
    try {
      const response = await fetch(`http://basic-web.dev.avc.web.usf.edu/${username}`, { method: 'GET' });
      if (response.status === 200) {
        const userData = await response.json();
        window.location.href = `main.html?id=${username}`; // Redirect to main page if user exists
      } else if (response.status === 404) {
        await createUser(username); // Create user if not found
      } else if (response.status === 500) {
        const errorData = await response.json();
        alert(`Internal server error: ${errorData.Error}`);
      } else {
        throw new Error('Error checking username');
      }
    } catch (error) {
      console.error('Error:', error);
      alert('Failed to check username. Please try again later.');
    }
  }

  // Create user on the server and redirect
  async function createUser(username) {
    try {
      const response = await fetch(`http://basic-web.dev.avc.web.usf.edu/${username}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }, 
        body: JSON.stringify({ score: 0 }) // Create user with initial score 0
      });

      if (response.status === 200 || response.status === 201) {
        const userData = await response.json();
        window.location.href = `main.html?id=${username}`; // Redirect after creating user
      } else if (response.status === 400) {
        const errorData = await response.json();
        alert(`Invalid request: ${errorData.Error}`);
      } else if (response.status === 500) {
        const errorData = await response.json();
        alert(`Internal server error: ${errorData.Error}`);
      } else {
        throw new Error('Error creating user');
      }
    } catch (error) {
      console.error('Error:', error);
      alert('Failed to create user. Please try again later.');
    }
  }
});
